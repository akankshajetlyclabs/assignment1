package com.app.entities;

import static com.app.entities.Main.mylist;

import static com.app.entities.Main.valueset;

public class Consumer implements Runnable {

	void get() {
		
		while (mylist.size() == 0) {
			try {
				mylist.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}	
		System.out.println("The removed number is:" + mylist.remove(0));
		mylist.notify();
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
	}

	public void run() {

		System.out.println("consumer is starting");
		synchronized (mylist) {

			while (valueset) {
				get();
			}
		}
	}
}
