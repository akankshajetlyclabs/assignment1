package com.app.entities;

abstract public class Car {
	
	
	public Car(int iD, String model_,int price) {
		
		ID = iD;
		this.model = model_;
		this.price=price;

	}
	int ID;
	String model;
	int price;
	int resaleValue;
	public String toString()
	{
		return  "\n carId \t" + this.ID + "\n carModel \t" + this.model+"\n car price\t"+this.price + "\n car resaleValue\t" + this.resaleValue;
	}

   
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public String getModel_no() {
		return model;
	}
	public void setModel_no(String model_no) {
		this.model = model_no;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getresalevalue() {
		return resaleValue;
	}
	public void setresalevalue(int resale_value) {
		this.resaleValue = resale_value;
	}
	
	
}
	
	
	

