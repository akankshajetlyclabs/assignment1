package com.example.sarthakjetly.assignment3;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity2 extends ActionBarActivity {
    int roll_no;
    String name;
    TestAdapter adapter;

    EditText editText1;
    EditText editText2     ;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity2);
         editText1= (EditText)findViewById(R.id.editName);
         editText2= (EditText)findViewById(R.id.editRoll);
         Bundle receiveBundle=   this.getIntent().getExtras();
        if(receiveBundle!=null) {
            final String receive1 = receiveBundle.getString("name");
            editText1.setText(String.valueOf(receive1));
            final int receive2 = receiveBundle.getInt("roll_no");
            editText2.setText(Integer.toString(receive2));
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main_activity2, menu);
        return true;
    }






   // @Override
   // public boolean onOptionsItemSelected(MenuItem item) {

        //int id = item.getItemId();


      //  if (id == R.id.action_settings) {
        //    return true;
      //  }

      //  return super.onOptionsItemSelected(item);
  //  }








public void onClick(View view)

{
    switch (view.getId())
    {

        case R.id.Save:
            String name;
            int roll_no;
            name=editText1.getText().toString();

            roll_no=Integer.parseInt(editText2.getText().toString());
            Intent intent=new Intent();
            intent.putExtra("name",name);
            intent.putExtra("roll_no",roll_no);
            setResult(RESULT_OK,intent);
            finish();




        case R.id.Cancel:
            setResult(RESULT_CANCELED);
            finish();

    }




}







}
