package com.example.sarthakjetly.assignment3;

import android.app.Activity;

import android.app.AlertDialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.jar.Attributes;
import java.util.zip.Inflater;


public class MainActivity extends Activity {


    TestAdapter adapter;
    ListView listView;
    TextView textView;

    Context context;
    EditText editText1, editText2;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.listView);


        final List<Student> list = new ArrayList<Student>();


        adapter = new TestAdapter(this, list);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view,
                                    final int position, long id) {

                LayoutInflater li = LayoutInflater.from(MainActivity.this);

                view = li.inflate(R.layout.phase2, null);
                final AlertDialog dialog = new AlertDialog.Builder(MainActivity.this).create();
                dialog.setView(view);
                dialog.setTitle("phase2");


                view.findViewById(R.id.Delete).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view)

                    {

                        adapter.list.remove(position);
                        adapter.notifyDataSetChanged();

                        dialog.dismiss();
                    }
                });

                view.findViewById(R.id.View).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view)

                    {

                        adapter.list.get(position);
                        adapter.notifyDataSetChanged();
                        dialog.dismiss();
                    }
                });
                view.findViewById(R.id.Edit).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view)

                    {





                        String name1 = adapter.list.get(position).name;
                        int roll = adapter.list.get(position).roll_no;




                        Bundle b = new Bundle();
                        Intent i = new Intent(MainActivity.this, MainActivity2.class);
                        b.putString("name", name1);
                        b.putInt("roll_no", roll);
                        i.putExtras(b);

                        startActivityForResult(i,10);

                        adapter.list.remove(position);

                             dialog.dismiss();

                    }
                });
                dialog.show();


                //  view.findViewById(R.id.Edit);
                // view.setOnClickListener(new  View.OnClickListener() {
                // public void onClick(View view)

                //  {
                //  editText1= (EditText)findViewById(R.id.editName);
                // editText2= (EditText)findViewById(R.id.editRoll);

                //    Intent intent = new Intent(this, MainActivity2.class);
                //   startActivityForResult(intent, 1);
                //  editText1.setText();


                // }
                // });

            }
        });
    }




    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    public void abc() {

        Intent intent = new Intent(this, MainActivity2.class);
        startActivityForResult(intent, 1);

    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {

                String name = data.getStringExtra("name");
                int roll_no = data.getIntExtra("roll_no", 0);
                adapter.list.add(new Student(name, roll_no));
                adapter.notifyDataSetChanged();

            }}
            if (requestCode == 10) {
                if (resultCode == RESULT_OK) {

               Bundle extras=data.getExtras();
                    String name = extras.getString("name");
                    int roll_no = extras.getInt("roll_no", 0);
                    adapter.list.add(new Student(name, roll_no));
                    adapter.notifyDataSetChanged();
                }


            else if (resultCode == RESULT_CANCELED) {


            }
        }
    }


    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bttnId:
                abc();
                break;

            case R.id.Delete:
                break;


            default:
                break;

        }
    }
}

