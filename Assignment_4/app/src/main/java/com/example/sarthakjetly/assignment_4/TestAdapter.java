package com.example.sarthakjetly.assignment_4;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;


    public class TestAdapter extends BaseAdapter {


        public List<String> list;
        Context context;

        public TestAdapter(   List<String> list,Context context){
            this.context=context;
            this.list=list;

        }

        public int getCount() {
            return list.size();
        }


        public View getView(int position, View convertView, ViewGroup parent) {

   LayoutInflater inflater = (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.list_items,null);
            TextView tv=(TextView) view.findViewById(R.id.textNum1);
            tv.setText(list.get(position));
            return view;


        }


        public long getItemId(int position) {
            return 0;
        }


        public Object getItem(int position) {
            return list.get(position);
        }


    }






